node ('docker') {
  stage("Cleanup") {
    deleteDir()
  }

  stage('Checkout') {
    echo "Checkout"
    checkout scm
  }

  def tag = sh(returnStdout: true, script: "git rev-parse --short HEAD").trim()
  def app_name = sh(returnStdout: true, script: "basename -s .git `git config --get remote.origin.url`").trim()
  def image
  def registry_url = "https://dr.i.symmetrical.ai"
  def registry_creds = "dr"

  // Get basename of current directory
  def basedir = WORKSPACE.substring(WORKSPACE.lastIndexOf('/') + 1, WORKSPACE.length())
  compose_name = ''

  // Now we need to have workaround for docker-compose bug which happens in base directory starts from hypen
  // We will calculate new compose project name and pass it directly to compose - to avoid loading it from basedir
  if ( basedir.substring(0,1 ) == "-" ) {
    compose_name="X" + basedir.substring(1,basedir.length())
  } else {
    compose_name=basedir
  }
  compose_name = compose_name.replaceAll("[.@_-]", "").toLowerCase()

  def venv_dir = compose_name + "_venv"

  stage('Cleaning unused docker resources') {
    sh "docker-compose -f docker-compose.pytest.yml down -v"
    sh "docker-compose -f docker-compose.newman.yml down -v"
  }

  stage('Build') {
    image = docker.build("${app_name}:${tag}")

    echo "Build number: ${env.BUILD_NUMBER}"
    echo "Tag: ${tag}"
  }

  stage('Testing Python internals with pytest'){
    try {
      echo "Testing with pytest"
      sh "mkdir -p tests/pytest"
      sh "docker-compose -f docker-compose.pytest.yml -p ${compose_name} build"
      sh "docker-compose -f docker-compose.pytest.yml -p ${compose_name} up --force-recreate --abort-on-container-exit"
    } catch (error) {
    } finally {
      def container_name = sh(returnStdout: true, script: "docker container ls -a | grep ${compose_name}_sut | awk '{print \044NF}'").trim()
      sh "docker cp ${container_name}:/app/tests/pytest/pytest_results.xml tests/pytest/"
      junit "tests/pytest/*.xml"
      sh "docker-compose -f docker-compose.pytest.yml -p ${compose_name} down -v"
    }
  }

  stage('API tests with newman') {
    try {
      echo "Testing API with newman"
      sh "docker-compose -f docker-compose.newman.yml -p ${compose_name} build"
      sh "docker-compose -f docker-compose.newman.yml -p ${compose_name} up --force-recreate --abort-on-container-exit"
    } catch (error) {
    } finally {
      def container_name = sh(returnStdout: true, script: "docker container ls -a | grep ${compose_name}_sut | awk '{print \044NF}'").trim()
      sh "docker cp ${container_name}:/app/tests/postman/newman/ tests/postman/"
      junit 'tests/postman/newman/*.xml'
      sh "docker-compose -f docker-compose.newman.yml -p ${compose_name} down -v"
    }
  }

  if(isOnMaster()) {
    stage("Promote") {
      docker.withRegistry("${registry_url}","${registry_creds}") {
        docker.build("${app_name}:${tag}").push("${tag}")
      }
    }

    stage("Deploy") {
      echo "Deploy step"
    }
  }
}

def isOnMaster() {
  return !env.BRANCH_NAME || env.BRANCH_NAME == "master";
}
